#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROWS1 4
#define COLS2 5
#define SHM_SIZE ROWS1*COLS2*sizeof(int)

// Fungsi untuk menghitung faktorial
void *factorial(void *num_ptr) {
    int i, num = *((int*) num_ptr);
    unsigned long long result = 1;

    // Hitung faktorial
    for(i=1; i<=num; i++) {
        result *= i;
    }

    pthread_exit((void*) result);
}

int main() {
    int i, j, k;
    int *result;
    pthread_t threads[ROWS1*COLS2];
    int thread_args[ROWS1*COLS2];
    void *thread_results[ROWS1*COLS2];

    // Mengakses shared memory
    key_t key = ftok("result", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    result = (int*) shmat(shmid, NULL, 0);

    // Hitung faktorial untuk setiap angka pada matriks menggunakan thread
    k = 0;
    for(i=0; i<ROWS1; i++) {
        for(j=0; j<COLS2; j++) {
            thread_args[k] = result[i*COLS2+j];
            pthread_create(&threads[k], NULL, factorial, &thread_args[k]);
            k++;
        }
    }

    // Tunggu semua thread selesai
    for(i=0; i<ROWS1*COLS2; i++) {
        pthread_join(threads[i], &thread_results[i]);
    }

    // Tampilkan hasil faktorial dalam format matriks
    printf("Hasil faktorial matriks:\n");
    k = 0;
    for(i=0; i<ROWS1; i++) {
        for(j=0; j<COLS2; j++) {
            printf("%llu ", (unsigned long long) thread_results[k]);
            k++;
        }
        printf("\n");
    }

    // Detach dari shared memory
    shmdt(result);

    return 0;
}

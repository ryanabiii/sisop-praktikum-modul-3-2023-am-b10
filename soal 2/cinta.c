#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 4
#define COLS2 5
#define SHM_SIZE ROWS1*COLS2*sizeof(int)

int main() {
    int i, j;
    int *result;

    // Mengakses shared memory
    key_t key = ftok("result", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    result = (int*) shmat(shmid, NULL, 0);

    // Tampilkan matriks hasil perkalian ke layar
    printf("Hasil perkalian matriks:\n");
    for(i=0; i<ROWS1; i++) {
        for(j=0; j<COLS2; j++) {
            printf("%d ", result[i*COLS2+j]);
        }
        printf("\n");
    }

    // Detach dari shared memory
    shmdt(result);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}

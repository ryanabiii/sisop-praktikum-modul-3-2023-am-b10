#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROWS1 4
#define COLS1 2
#define ROWS2 2
#define COLS2 5
#define SHM_SIZE ROWS1*COLS2*sizeof(int)

int main() {
    int i, j, k;
    int m1[ROWS1][COLS1], m2[ROWS2][COLS2];
    int *result;

    // Isi matriks pertama dengan angka random 1-5
    for(i=0; i<ROWS1; i++) {
        for(j=0; j<COLS1; j++) {
            m1[i][j] = rand() % 5 + 1;
        }
    }

    // Isi matriks kedua dengan angka random 1-4
    for(i=0; i<ROWS2; i++) {
        for(j=0; j<COLS2; j++) {
            m2[i][j] = rand() % 4 + 1;
        }
    }

    // Alokasi shared memory
    key_t key = ftok("result", 'R');
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    result = (int*) shmat(shmid, NULL, 0);

    // Perkalian matriks
    for(i=0; i<ROWS1; i++) {
        for(j=0; j<COLS2; j++) {
            result[i*COLS2+j] = 0;
            for(k=0; k<COLS1; k++) {
                result[i*COLS2+j] += m1[i][k] * m2[k][j];
            }
        }
    }

    // Tampilkan matriks hasil perkalian ke layar
    printf("Hasil perkalian matriks:\n");
    for(i=0; i<ROWS1; i++) {
        for(j=0; j<COLS2; j++) {
            printf("%d ", result[i*COLS2+j]);
        }
        printf("\n");
    }

    // Detach dan dealokasi shared memory
    shmdt(result);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MAX_MSG_SIZE 1024

// Struct untuk pesan yang dikirim oleh user
struct msgbuf {
    long mtype;
    char mtext[MAX_MSG_SIZE];
};

// Deklarasi fungsi dekripsi
void decrypt(char *str);

int main() {
    key_t key;
    int msgid, ret;
    struct msgbuf msg;

    // Membuat kunci untuk message queue
    key = ftok("stream.c", 'A');
    if (key == -1) {
        perror("Error in ftok");
        exit(EXIT_FAILURE);
    }

    // Membuat message queue dengan hak akses 0600 (read-write)
    msgid = msgget(key, 0600 | IPC_CREAT);
    if (msgid == -1) {
        perror("Error in msgget");
        exit(EXIT_FAILURE);
    }

    printf("Waiting for messages from user...\n");

    // Menerima pesan dari user dan memproses perintah
    while (1) {
        ret = msgrcv(msgid, &msg, MAX_MSG_SIZE, 1, 0);
        if (ret == -1) {
            perror("Error in msgrcv");
            exit(EXIT_FAILURE);
        }

        printf("Received message: %s\n", msg.mtext);

        if (strcmp(msg.mtext, "DECRYPT") == 0) {
            // Buka file song-playlist.json
            FILE *f = fopen("song-playlist.json", "r");
            if (f == NULL) {
                perror("Error opening file");
                exit(EXIT_FAILURE);
            }

            // Baca isi file song-playlist.json
            char buffer[1024];
            size_t n;
            while ((n = fread(buffer, 1, sizeof buffer, f)) > 0) {
                // Dekripsi isi file
                decrypt(buffer);

                // Tulis isi file yang telah didekripsi ke playlist.txt
                FILE *out = fopen("playlist.txt", "a");
                if (out == NULL) {
                    perror("Error opening file");
                    exit(EXIT_FAILURE);
                }
                fwrite(buffer, 1, n, out);
                fclose(out);
            }

            fclose(f);

            // Urutkan isi file playlist.txt secara alfabetis
            char command[1024];
            sprintf(command, "sort -o playlist.txt playlist.txt");
            system(command);

            printf("File decryption and sorting complete!\n");
        }
    }

    return 0;
}

void decrypt(char *str) {
    // Implementasi fungsi dekripsi sesuai dengan metodenya
    for (int i = 0; str[i] != '\0'; ++i) {
        if (str[i] >= 'A' && str[i] <= 'Z') {
            str[i] = (char)(((str[i] - 'A') + 13) % 26 + 'A');
        } else if (str[i] >= '0' && str[i] <= '9') {
		  str[i] = (char)(((str[i] - '0') + 5) % 10 + '0');
		}
	}
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <sys/types.h>

#define MAX_MSG_SIZE 1024

// Struct untuk pesan yang akan dikirim ke stream.c
struct msgbuf {
    long mtype;
    char mtext[MAX_MSG_SIZE];
};

int main() {
    key_t key;
    int msgid, ret;
    struct msgbuf msg;

    // Membuat kunci untuk message queue
    key = ftok("stream.c", 'A');
    if (key == -1) {
        perror("Error in ftok");
        exit(EXIT_FAILURE);
    }

    // Mendapatkan ID message queue
    msgid = msgget(key, 0600);
    if (msgid == -1) {
        perror("Error in msgget");
        exit(EXIT_FAILURE);
    }

    // Kirim pesan ke stream.c
    strcpy(msg.mtext, "DECRYPT");
    msg.mtype = 1;
    ret = msgsnd(msgid, &msg, strlen(msg.mtext) + 1, 0);
    if (ret == -1) {
        perror("Error in msgsnd");
        exit(EXIT_FAILURE);
    }

    printf("Sent message to stream.c: %s\n", msg.mtext);

    return 0;
}


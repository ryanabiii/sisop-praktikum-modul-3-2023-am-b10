#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

#define EXTENSIONS_FILE "extensions.txt"
#define LOG_FILE "log.txt"

int main() {
    FILE *fp;
    char *line = NULL;
    char extensions[100][100];
    size_t len = 0;
    ssize_t read;
    int count = 0;

    // Open log file
    fp = fopen(LOG_FILE, "r");
    if (fp == NULL) {
        printf("Failed to open file: %s\n", LOG_FILE);
        return 1;
    }

    // Loop through lines in log file
    while ((read = getline(&line, &len, fp)) != -1) {
        // Count occurrences of "ACCESSED"
        char *check = strstr(line, "ACCESSED");
        while (check != NULL) {
            count++;
            check = strstr(check + 1, "ACCESSED");
        }
    }

    printf("ACCESSED : %d\n", count);

    fclose(fp);

    DIR *dir;
    struct dirent *entry;
    struct stat filestat;
    char *dir_path = "categorized/";

    // Open directory
    dir = opendir(dir_path);
    if (dir == NULL) {
        return 1;
    }

    int i = 0;

    // Loop through directory entries
    while ((entry = readdir(dir)) != NULL) {
        // Ignore files that start with .
        if (entry->d_name[0] == '.') {
            continue;
        }
        // Construct full path of entry
        char full_path[256];
        snprintf(full_path, sizeof(full_path), "%s%s", dir_path, entry->d_name);

        strcpy(extensions[i], entry->d_name);

        // Print name of entry
        printf("%s\n", entry->d_name);
        i++;
    }

    system("tree categorized");

    closedir(dir);
    printf("\n");

    // Open extensions file
    // fp = fopen(EXTENSIONS_FILE, "r");
    // if (fp == NULL) {
    //     return 1;
    // }

    // int i = 0;

    // // Loop through lines in extensions file
    // while ((read = getline(&line, &len, fp)) != -1) {
    //     // Remove newline character
    //     line[strlen(line)-2] = '\0';

    //     strcpy(extensions[i], line);
    //     i++;
    // }

    // fclose(fp);

    for(int j = 0; j < i-1; j++) {
        int dircount = 0;
        chdir("categorized");
        dir = opendir(extensions[j]);
        if (dir == NULL) {
            return 1;
        }

        // Loop through directory entries
        while ((entry = readdir(dir)) != NULL) {
            // Ignore files that start with .
            if (entry->d_name[0] == '.') {
                continue;
            }
            // Construct full path of entry
            char full_path[256];
            snprintf(full_path, sizeof(full_path), "%s%s", dir_path, entry->d_name);

            // Print name of entry
            printf("DIR %s : %s\n", extensions[j], entry->d_name);
            dircount++;
        }
        printf("\n%s : %d\n\n", extensions[j], dircount);
    }

    closedir(dir);

    return 0;
}
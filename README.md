# sisop-praktikum-modul-3-2023-AM-B10

Anggota :
| Nama                         | NRP        |
|------------------------------|------------|
|Raden Pandu Anggono Rasyid    | 5025201024 |
|Ryan Abinugraha               | 5025211178 |
|Abhinaya Radiansyah Listiyanto| 5025211173 |
## Penjelasan No 1
Code untuk lossless.c 
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_CHAR 26

// Struktur node untuk Huffman tree
struct Huffman_Tree {
    int frekuensi;
    char karakter;
    struct Huffman_Tree 
        *left, *right;
};

// Fungsi untuk membuat node Huffman
struct Huffman_Tree* Create_Huffman(char character, int frekuensi) {
    struct Huffman_Tree* node = (struct Huffman_Tree*)malloc(sizeof(struct Huffman_Tree));
    node->character = character;
    node->frekuensi = frekuensi;
    node->left = node->right = NULL;
    return node;
}

// Fungsi untuk menghitung frekuensi kemunculan huruf pada file
void Calculate_Frequency(const char* filename, int frekuensi[MAX_CHAR]) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("File not found.\n");
        exit(1);
    }
    
    char c;
    while ((c = fgetc(file)) != EOF) {
        if (c >= 'A' && c <= 'Z') {
            frekuensi[c - 'A']++;
        }
    }
    
    fclose(file);
}

// Fungsi untuk mengirim frekuensi kemunculan huruf ke child process menggunakan pipe
void Sent_Frequency(int frekuensi[MAX_CHAR], int pipe_fd) {
    close(pipe_fd[0]);
    write(pipe_fd[1], frekuensi, sizeof(int) * MAX_CHAR);
    close(pipe_fd[1]);
}

// Fungsi untuk menerima frekuensi kemunculan huruf dari parent process menggunakan pipe
void Received_Frequency(int frekuensi[MAX_CHAR], int pipe_fd) {
    close(pipe_fd[1]);
    read(pipe_fd[0], frekuensi, sizeof(int) * MAX_CHAR);
    close(pipe_fd[0]);
}

// Fungsi untuk membuat Huffman tree berdasarkan frekuensi kemunculan huruf
struct Huffman_Tree* buildHuffmanTree(int frekuensi[MAX_CHAR]) {
    // Implementasi pembangunan Huffman tree
    // ...
}

// Fungsi untuk melakukan kompresi menggunakan algoritma Huffman
void compress(const char* filename, const char* compressedFilename, struct Huffman_Tree* root) {
    // Implementasi kompresi file dengan menggunakan Huffman tree
    // ...
}

int main() {
    const char* filename = "inputfile.txt";
    const char* compressedFilename = "compressedfile.txt";
    int frekuensi[MAX_CHAR] = {0};
    
    // Parent process
    Calculate_Frequency(filename, frekuensi);
    
    int pipe_fd[2];
    pipe(pipe_fd);
    
    pid_t pid = fork();
    
    if (pid == 0) {
        // Child process
        close(pipe_fd[1]);
        Received_Frequency(frekuensi, pipe_fd);
        
        struct Huffman_Tree* root = buildHuffmanTree(frekuensi);
        compress(filename, compressedFilename, root);
        
        close(pipe_fd[0]);
    } else if (pid > 0) {
        // Parent process
        close(pipe_fd[0]);
        Sent_Frequency(frekuensi, pipe_fd);
        
        wait(NULL); // Menunggu child process selesai
        
        // Baca Huffman tree dari file terkompresi
        // Dekompresi file dan hitung jumlah bit setelah kompresi
        
        close(pipe_fd[1]);
    } else {
        printf("Fork failed.\n");
        return 1;
    }
    
    return 0;
}

```
Pada awal buat structur node untuk Huffman tree diikuti dengan fungsinya untuk membuat node Huffman

```
struct Huffman_Tree* Create_Huffman(char character, int frekuensi) {
    struct Huffman_Tree* node = (struct Huffman_Tree*)malloc(sizeof(struct Huffman_Tree));
    node->character = character;
    node->frekuensi = frekuensi;
    node->left = node->right = NULL;
    return node;
}
 ```
 Setelah itu buat fungsi untuk menghitung frekuensi kemunculan huruf pada file menggunakan if else statement

 Selanjutnya buat fungsi untuk mengirim frekuensi kemunculan huruf dan fungsi untuk menerima frekuensi menggunakan metode pipe
 
 Fungsi untuk mengirim frekuensi
 ```
 void Sent_Frequency(int frekuensi[MAX_CHAR], int pipe_fd) {
    close(pipe_fd[0]);
    write(pipe_fd[1], frekuensi, sizeof(int) * MAX_CHAR);
    close(pipe_fd[1]);
}

 ```
 Fungsi untuk menerima frekuensi
```
void Received_Frequency(int frekuensi[MAX_CHAR], int pipe_fd) {
    close(pipe_fd[1]);
    read(pipe_fd[0], frekuensi, sizeof(int) * MAX_CHAR);
    close(pipe_fd[0]);
}
```
Setelah file didapatkan, lakukan kompresi menggunakan algoritma Huffman

Setelah semuanya telah dibuat, masuk ke main fungsi untuk menjalankan semua perintahnya

int main()
```
int main() {
    const char* filename = "inputfile.txt";
    const char* compressedFilename = "compressedfile.txt";
    int frekuensi[MAX_CHAR] = {0};
    
    // Parent process
    Calculate_Frequency(filename, frekuensi);
    
    int pipe_fd[2];
    pipe(pipe_fd);
    
    pid_t pid = fork();
    
    if (pid == 0) {
        // Child process
        close(pipe_fd[1]);
        Received_Frequency(frekuensi, pipe_fd);
        
        struct Huffman_Tree* root = buildHuffmanTree(frekuensi);
        compress(filename, compressedFilename, root);
        
        close(pipe_fd[0]);
    } else if (pid > 0) {
        // Parent process
        close(pipe_fd[0]);
        Sent_Frequency(frekuensi, pipe_fd);
        
        wait(NULL); // Menunggu child process selesai
        
        // Baca Huffman tree dari file terkompresi
        // Dekompresi file dan hitung jumlah bit setelah kompresi
        
        close(pipe_fd[1]);
    } else {
        printf("Fork failed.\n");
        return 1;
    }
    
    return 0;
}

```
## Penjelasan No 2
Dalam soal no 2 diminta 3 program yang dimana program pertama membuat program C untuk menghitung perkalian matrix dengan ukuran pertama 4X2 dan ukuran kedua matrik 2X5 dan memasukkan angka dengan angak random 1-4 dan 1-5, program ini nanti ada kaitannya dengan program kedua dengan menggunakan shared memory.

```
    // Isi matriks pertama dengan angka random 1-5
    for(i=0; i<ROWS1; i++) {
        for(j=0; j<COLS1; j++) {
            m1[i][j] = rand() % 5 + 1;
        }
    }

    // Isi matriks kedua dengan angka random 1-4
    for(i=0; i<ROWS2; i++) {
        for(j=0; j<COLS2; j++) {
            m2[i][j] = rand() % 4 + 1;
        }
    }

    // Alokasi shared memory
    key_t key = ftok("result", 'R');
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    result = (int*) shmat(shmid, NULL, 0);
```
kode diatas untuk mendapatkan nilai random menggunakan fungsi rand() dan shared memory unutuk menyimpan hasilnya nanti supaya bisa diakses di program selanjutnya.
```
    // Detach dan dealokasi shared memory
    shmdt(result);
    shmctl(shmid, IPC_RMID, NULL);
```
dan ini untuk mengakses shared memory yang ingin diperlukan untuk dapat mengambil hasil matrix tersebut.

Pada program ke dua diberikan nama program cinta.c yang dimana mengeluarkan hasil dari program pertama dengan cara shared memory jadi dalam program ini tidak perlu mencari dengan perkalian melainkan hanya mengambil hasil pada program pertama.
```
   // Mengakses shared memory
    key_t key = ftok("result", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    result = (int*) shmat(shmid, NULL, 0);
```
fungsi ftok() untuk menghasilkan key untuk shared memory dan untuk mengaskes memory digunakan fungsi shmget().
```
 // Detach dari shared memory
    shmdt(result);
    shmctl(shmid, IPC_RMID, NULL);
```
smdt() untuk memutus shared memory yang digunakan.

Pada program ketiga diminta menamai dengan program sisop.c yang isinya menampilkan hasil matrik tersebut dengan mencari nilai faktorialnya saja dan menggunakan fungsi thread dan multithreading.
```
    // Mengakses shared memory
    key_t key = ftok("result", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    result = (int*) shmat(shmid, NULL, 0);
```
sama seperti sebulmnya kita mengakses shared memorynya terlebih dahulu.
```
    // Hitung faktorial untuk setiap angka pada matriks menggunakan thread
    k = 0;
    for(i=0; i<ROWS1; i++) {
        for(j=0; j<COLS2; j++) {
            thread_args[k] = result[i*COLS2+j];
            pthread_create(&threads[k], NULL, factorial, &thread_args[k]);
            k++;
        }
    }

    // Tunggu semua thread selesai
    for(i=0; i<ROWS1*COLS2; i++) {
        pthread_join(threads[i], &thread_results[i]);
    }
```
program ini menggunakan fungsi pthread_create() untuk membuat thread untuk setiap angka pada matriks dan menghitung faktorialnya menggunakan fungsi factorial(). Setelah semua thread selesai dijalankan, program menampilkan hasil faktorial dalam format matriks.

## Penjelasan No 3
Code stream.c
```
key = ftok("stream.c", 'A');
msgid = msgget(key, 0600);

```
membuat message queue dengan memanggil ftok() untuk membuat kunci unik berdasarkan nama file dan suatu karakter acak. Kemudian, program menggunakan kunci tersebut untuk memanggil msgget() dan mendapatkan ID message queue yang digunakan untuk mengirim dan menerima pesan.
```
ret = msgrcv(msgid, &msg, MAX_MSG_SIZE, 0, 0);
```
Setelah message queue dibuat, program akan terus menerus menjalankan loop yang memanggil msgrcv() untuk menerima pesan dari message queue. Fungsi msgrcv() akan memblokir program sampai ada pesan yang masuk.
```
if (strcmp(msg.mtext, "DECRYPT") == 0) {
    decryptPlaylist();
}
```
Berfungsi untuk memproses pesan

Program user.c
```
strcpy(msg.mtext, "DECRYPT");
msg.mtype = 1;
ret = msgsnd(msgid, &msg, strlen(msg.mtext) + 1, 0);
```
berfugngsi untuk mengisi pesan yang akan dikirim ke message queue dengan memasukkan pesan ke dalam field mtext pada struktur msgbuf. dan msgsnd() untuk mengirim pesan tersebut ke message queue.

## Penjelasan No 4

Code untuk categorize.C
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>

#define EXTENSIONS_FILE "extensions.txt"
#define LOG_FILE "log.txt"

int main() {
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    FILE *fp;
    FILE *fp_log;
    char extensions[100][100];
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    // Open extensions file
    fp = fopen(EXTENSIONS_FILE, "r");
    if (fp == NULL) {
        return 1;
    }

    // Open log file
    fp_log = fopen(LOG_FILE, "w");
    if (fp_log == NULL) {
        return 1;
    }

    fprintf(fp_log, "%s ACCESSED extensions.txt\n", datetime);

    mkdir("categorized", 0777);
    fprintf(fp_log, "%s MADE categorized\n", datetime);

    int i = 0;

    // Loop through lines in extensions file
    while ((read = getline(&line, &len, fp)) != -1) {
        // Remove newline character
        line[strlen(line)-2] = '\0';

        strcpy(extensions[i], line);
        printf("%s\n", line);

        chdir("categorized");
        fprintf(fp_log, "%s ACCESSED categorized\n", datetime);

        mkdir(line, 0777);
        fprintf(fp_log, "%s MADE categorized/%s\n", datetime, line);
        i++;
    }

    chdir("categorized");
    fprintf(fp_log, "%s ACCESSED categorized\n", datetime);
    mkdir("other", 0777);
    fprintf(fp_log, "%s MADE categorized/other\n", datetime);

    fclose(fp);

    chdir("..");

    DIR *dir;
    struct dirent *ent;
    char *src_dir = "/home/rep/Project/Bash/Shift-3/files";

    // Open source directory
    if ((dir = opendir(src_dir)) == NULL) {
        return 1;
    }
    fprintf(fp_log, "%s ACCESSED files\n", datetime);

    // Loop through files in source directory
    while ((ent = readdir(dir)) != NULL) {
        // Ignore files that start with .
        if (ent->d_name[0] == '.') {
            continue;
        }

        // Get file extension
        char *extension = strrchr(ent->d_name, '.');
        if (extension == NULL) {
            continue;
        }

        // Move file to destination directory
        char src[256];
        snprintf(src, sizeof(src), "%s/%s", src_dir, ent->d_name);

        char dest[256];
        snprintf(dest, sizeof(dest), "%s/%s/%s", "/home/rep/Project/Bash/Shift-3/categorized", extension + 1, ent->d_name);

        chdir("categorized");
        fprintf(fp_log, "%s ACCESSED categorized\n", datetime);

        if (rename(src, dest)) {
            printf("Moved file: %s -> %s\n", src, dest);
            fprintf(fp_log, "%s MOVED %s > %s\n", datetime, src, dest);
        }
    }

    closedir(dir);

    return 0;
}
```
Code untuk logchecker.c 
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

#define EXTENSIONS_FILE "extensions.txt"
#define LOG_FILE "log.txt"

int main() {
    FILE *fp;
    char *line = NULL;
    char extensions[100][100];
    size_t len = 0;
    ssize_t read;
    int count = 0;

    // Open log file
    fp = fopen(LOG_FILE, "r");
    if (fp == NULL) {
        printf("Failed to open file: %s\n", LOG_FILE);
        return 1;
    }

    // Loop through lines in log file
    while ((read = getline(&line, &len, fp)) != -1) {
        // Count occurrences of "ACCESSED"
        char *check = strstr(line, "ACCESSED");
        while (check != NULL) {
            count++;
            check = strstr(check + 1, "ACCESSED");
        }
    }

    printf("ACCESSED : %d\n", count);

    fclose(fp);

    DIR *dir;
    struct dirent *entry;
    struct stat filestat;
    char *dir_path = "categorized/";

    // Open directory
    dir = opendir(dir_path);
    if (dir == NULL) {
        return 1;
    }

    int i = 0;

    // Loop through directory entries
    while ((entry = readdir(dir)) != NULL) {
        // Ignore files that start with .
        if (entry->d_name[0] == '.') {
            continue;
        }
        // Construct full path of entry
        char full_path[256];
        snprintf(full_path, sizeof(full_path), "%s%s", dir_path, entry->d_name);

        strcpy(extensions[i], entry->d_name);

        // Print name of entry
        printf("%s\n", entry->d_name);
        i++;
    }

    system("tree categorized");

    closedir(dir);
    printf("\n");

    // Open extensions file
    // fp = fopen(EXTENSIONS_FILE, "r");
    // if (fp == NULL) {
    //     return 1;
    // }

    // int i = 0;

    // // Loop through lines in extensions file
    // while ((read = getline(&line, &len, fp)) != -1) {
    //     // Remove newline character
    //     line[strlen(line)-2] = '\0';

    //     strcpy(extensions[i], line);
    //     i++;
    // }

    // fclose(fp);

    for(int j = 0; j < i-1; j++) {
        int dircount = 0;
        chdir("categorized");
        dir = opendir(extensions[j]);
        if (dir == NULL) {
            return 1;
        }

        // Loop through directory entries
        while ((entry = readdir(dir)) != NULL) {
            // Ignore files that start with .
            if (entry->d_name[0] == '.') {
                continue;
            }
            // Construct full path of entry
            char full_path[256];
            snprintf(full_path, sizeof(full_path), "%s%s", dir_path, entry->d_name);

            // Print name of entry
            printf("DIR %s : %s\n", extensions[j], entry->d_name);
            dircount++;
        }
        printf("\n%s : %d\n\n", extensions[j], dircount);
    }

    closedir(dir);

    return 0;
}
```
Code untuk unzip.c
```
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	system("curl -L \"https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp\" -o hehe.zip");
	system("unzip hehe.zip");

	return 0;
}
```

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_CHAR 26

// Struktur node untuk Huffman tree
struct Huffman_Tree {
    int frekuensi;
    char karakter;
    struct Huffman_Tree 
        *left, *right;
};

// Fungsi untuk membuat node Huffman
struct Huffman_Tree* Create_Huffman(char character, int frekuensi) {
    struct Huffman_Tree* node = (struct Huffman_Tree*)malloc(sizeof(struct Huffman_Tree));
    node->character = character;
    node->frekuensi = frekuensi;
    node->left = node->right = NULL;
    return node;
}

// Fungsi untuk menghitung frekuensi kemunculan huruf pada file
void Calculate_Frequency(const char* filename, int frekuensi[MAX_CHAR]) {
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        printf("File not found.\n");
        exit(1);
    }
    
    char c;
    while ((c = fgetc(file)) != EOF) {
        if (c >= 'A' && c <= 'Z') {
            frekuensi[c - 'A']++;
        }
    }
    
    fclose(file);
}

// Fungsi untuk mengirim frekuensi kemunculan huruf ke child process menggunakan pipe
void Sent_Frequency(int frekuensi[MAX_CHAR], int pipe_fd) {
    close(pipe_fd[0]);
    write(pipe_fd[1], frekuensi, sizeof(int) * MAX_CHAR);
    close(pipe_fd[1]);
}

// Fungsi untuk menerima frekuensi kemunculan huruf dari parent process menggunakan pipe
void Received_Frequency(int frekuensi[MAX_CHAR], int pipe_fd) {
    close(pipe_fd[1]);
    read(pipe_fd[0], frekuensi, sizeof(int) * MAX_CHAR);
    close(pipe_fd[0]);
}

// Fungsi untuk membuat Huffman tree berdasarkan frekuensi kemunculan huruf
struct Huffman_Tree* buildHuffmanTree(int frekuensi[MAX_CHAR]) {
    // Implementasi pembangunan Huffman tree
    // ...
}

// Fungsi untuk melakukan kompresi menggunakan algoritma Huffman
void compress(const char* filename, const char* compressedFilename, struct Huffman_Tree* root) {
    // Implementasi kompresi file dengan menggunakan Huffman tree
    // ...
}

int main() {
    const char* filename = "inputfile.txt";
    const char* compressedFilename = "compressedfile.txt";
    int frekuensi[MAX_CHAR] = {0};
    
    // Parent process
    Calculate_Frequency(filename, frekuensi);
    
    int pipe_fd[2];
    pipe(pipe_fd);
    
    pid_t pid = fork();
    
    if (pid == 0) {
        // Child process
        close(pipe_fd[1]);
        Received_Frequency(frekuensi, pipe_fd);
        
        struct Huffman_Tree* root = buildHuffmanTree(frekuensi);
        compress(filename, compressedFilename, root);
        
        close(pipe_fd[0]);
    } else if (pid > 0) {
        // Parent process
        close(pipe_fd[0]);
        Sent_Frequency(frekuensi, pipe_fd);
        
        wait(NULL); // Menunggu child process selesai
        
        // Baca Huffman tree dari file terkompresi
        // Dekompresi file dan hitung jumlah bit setelah kompresi
        
        close(pipe_fd[1]);
    } else {
        printf("Fork failed.\n");
        return 1;
    }
    
    return 0;
}
